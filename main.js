const express = require('express');
const jwt = require('jsonwebtoken');
const { check , validationResult } = require('express-validator/check');


const app = express();
// mock user 
const user = {
    id : 1,
    name : "masad",
    email : "masadashraf@ymail.com",
}

app.get('/api', (req, res) => {
    res.json({
        message : "Welcome to restaurant application (Node Server)"
    });
});

app.post('/api/login', (req, res) => {
    jwt.sign({user}, 'thisIsMySecretKey', (err, token) =>{
        res.json({
            status : 200,
            message : "Success",
            data : user,
            token
        });
    })
});



app.listen(3000, () => { 
    console.log('Server is connected on port 3000')
});